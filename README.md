## Getting started

Pustaka-booking merupakan aplikasi reservasi atau booking peminjaman buku pada sebuah perpustakaan yang dilakukan secara online dengan melibatkan anggota, admin dan aplikasi pustaka booking. Aplikasi pustaka booking ini dibagi menjadi 2 jenis tampilan yaitu tampilan backend dan frontend.

Alur logika sistem booking pustaka ini yaitu seseorang yang ingin melakukan booking diharuskan mendaftar menjadi anggota terlebih dahulu, selanjutnya ketika sudah menjadi anggota, baru dapat melakukan booking terhadap buku yang akan dipinjam.

Setelah melakukan booking, anggota diharuskan mengambil buku yang telah dibooking dengan cara datang langsung ke perpustakaan dalam waktu 1x24 jam. Kemudian konfirmasi ke petugas
atau admin untuk diambilkan buku yang telah dibooking berdasarkan bukti booking melalui
aplikasi.


## Sceenshot

![Admin Panel](https://gitlab.com/ariansyah-net/pustaka-booking/-/raw/main/assets/img/ss-1.png "Admin Dashboard")


![Admin Panel](https://gitlab.com/ariansyah-net/pustaka-booking/-/raw/main/assets/img/ss-2.png "Admin Dashboard")

## Installation

- Silahkan clone atau download aplikasi pustaka booking ini
- Import pustaka.sql kedalam dbms anda
- Lakukan konfigurasi base_url dan database pada direktori aplikasi
- Enjoy

## Tested

Aplikasi Pustaka Booking ini dibuat dengan framework CodeIgniter 3 dan Bootstrap 5 dan telah diuji pada sistem operasi linux dan lampp dengan PHP versi 8.0

**ADMIN**

- http://localhost/pustaka/auth
- Username : arian@gmail.com
- Password : 1212

**MEMBER**
- http://localhost/pustaka/
- Username : jono@gmail.com
- Password : 1212


## Project status

- Dibuat untuk melengkapi matakuliah web programming.
- Di publish sebagai media pembelajaran.
- Special to class 12.8A.07

## Authors
Arian

## Thanks to

- CodeIgniter 3.1.13
- SBAdmin
- Bootstrap
- DomPDF
- Font Awesome
