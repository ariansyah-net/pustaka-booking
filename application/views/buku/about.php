<div class="container">
	<div class="card shadow border-0 m-3">
		<div class="card-header fw-bold bg-secondary text-white"><h5><?= $title; ?></h5></div>
		<div class="card-body p-4 text-justify">
			<p class="pt-1"><strong>Pustaka Booking</strong> merupakan sebuah aplikasi reservasi atau booking peminjaman buku pada sebuah perpustakaan yang dilakukan secara online dengan melibatkan anggota, admin dan aplikasi pustaka booking. Aplikasi pustaka booking ini dibagi menjadi 2 jenis tampilan yaitu tampilan bagian backend dan tampilan frontend.</p>
			<p class="pt-1">
			Aplikasi pustaka booking adalah aplikasi berbasis web yang dibuat untuk booking sebuah buku untuk dipinjam. Tujuan dibuatkannya aplikasi ini agar memudahkan para pengguna dalam mencari buku dan ingin meminjam buku tersebut. Jadi sebelum datang ke
			perpustakaan dan meminjam sebuah buku, pengguna bisa mencari terlebih dahulu buku yang akan dipinjam untuk memastikan bahwa di perpustakaan yang akan dia kunjungi benar bahwa buku tersebut tersedia.</p>
			<p class="pt-1">
			Alur logika sistem booking pustaka ini yaitu seseorang yang ingin melakukan booking diharuskan mendaftar menjadi anggota terlebih dahulu, selanjutnya ketika sudah menjadi anggota, baru dapat melakukan booking terhadap buku yang akan dipinjam. Setelah melakukan booking, anggota diharuskan mengambil buku yang telah dibooking dengan cara
			datang langsung ke perpustakaan dalam waktu 1x24 jam. Kemudian konfirmasi ke petugas atau admin untuk diambilkan buku yang telah dibooking berdasarkan bukti booking melalui aplikasi.</p>
			<p class="pt-1">
			Pustaka booking secara utuh memiliki kebutuhan sebagai berikut:</p>
			<p class="font-monospace fw-bold">Kebutuhan User:</p>
			<ol>
				<li>Admin</li>
					<ul>
						<li>Seorang admin dapat login ke dalam aplikasi pustaka-booking.</li>
						<li>Seorang admin dapat menambah, melihat, mengubah, dan menghapus, data buku pada aplikasi pustaka-booking.</li>
						<li>Seorang admin dapat melihat data booking dan melanjutkan proses booking sampai buku dipinjam.</li>
						<li>Seorang admin dapat memproses pengembalian buku yang sudah selesai dipinjam.</li>
					</ul>
				<li>Pengunjung</li>
					<ul>
						<li>Pengunjung dapat melihat-lihat data buku yang ada pada palikasi pustaka-booking</li>
						<li>Pengunjung dapat melakukan registrasi untuk menjadi anggota pustaka-booking</li>
						<li>Pengunjung dapat memberikan komentar melalui buku tamu</li>
					</ul>
				<li>Anggota</li>
					<ul>
						<li>Anggota dapat login ke dalam sistem aplikasi pustaka-booking.</li>
						<li>Anggota dapat melakukan booking terhadap buku yang ada pada aplikasi pustaka-booking</li>
						<li>Anggota dapat mencetak bukti booking buku untuk dibawa ketika akan mengambil buku yang dibooking.</li>
					</ul>
			</ol>

			<hr>

			<p class="font-monospace fw-bold">Kebutuhan Sistem:</p>
			<ol>
				<li>Sistem akan melakukan validasi username dan password ketika ada seorang user yang melakukan login</li>
				<li>Sistem juga melakukan validasi data buku dan anggota yang akan diinput ke dalam database</li>
				<li>Sistem akan memblokir dan memberikan notif apabila ada seorang pengunjung yang meng-klik tombol booking tanpa melakukan login terlebih dahulu.</li>
				<li>Sistem akan menghapus secara otomatis data booking yang sudah lewat dari 1 x 24 jam</li>
				<li>Sistem dapat mengkalkulasi denda yang harus dibayarkan ketika ada anggota yang terlambat mengembalikan buku.</li>
				<li>Sistem dapat menampilkan buku yang dicari oleh user berdasarkan kriteria pencarian.</li>
			</ol>

			<p class="pt-4 text-muted small">Ditulis oleh: <i class="far fa-user-circle ps-2"></i> Ariansyah

		</div>
	</div>

</div>