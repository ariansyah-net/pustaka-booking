<div class="x_panel" align="center">

<div class="row col-8">
  <div class="text-center">
    <img class="rounded" style="max-width: 220px;" src="<?php echo base_url(); ?>assets/img/upload/<?= $gambar; ?>">
  </div>

<div class="my-4">
<h5 class="text-center"><?= $judul; ?></h5>


<div class="table-responsive mt-4">
  <table class="table table-hover">
    <tr>
    <th nowrap>Pengarang: </th>
    <td nowrap><?= $pengarang ?></td>
    <td>&nbsp;</td>
    <th>Kategori: </th>
    <td><?= $kategori ?></td>
    </tr>
    <tr>
    <th nowrap>Penerbit: </th>
    <td><?= $penerbit ?></td>
    <td>&nbsp;</td>
    <th>Dipinjam: </th>
    <td><?= $dipinjam ?></td>

    </tr>
    <tr>
    <th nowrap>Tahun Terbit: </th>
    <td><?= substr($tahun, 0, 4) ?></td>
    <td>&nbsp;</td>
    <th>Dibooking: </th>
    <td><?= $dibooking ?></td>
    </tr>
    <tr>
    <th>ISBN: </th>
    <td><?= $isbn ?></td>
    <td>&nbsp;</td>
    <th>Tersedia: </th>
    <td><?= $stok ?></td>
    </tr>
  </table>
</table>


<div class="d-flex justify-content-between pt-3">
  <button class="btn btn-outline-secondary" onclick="window.history.go(-1)"> <i class="fas fa-arrow-left"></i> </button>

  <a class="btn btn-primary" href="<?= base_url('booking/tambahBooking/' . $id); ?>"> <i class="fas fa-shopping-cart"></i> Booking</a>
</div>

</div>
</div>
</div>

</div>