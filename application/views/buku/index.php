<?= $this->session->flashdata('message'); ?>

<div class="row row-cols-1 row-cols-md-4 g-4 mt-4">
  
  <?php foreach ($buku as $buku) { ?>

    <div class="col mb-5">
      <div class="card shadow border-0">
        <a href="<?= base_url('home/detailBuku/' . $buku->id); ?>">
          <img class="img-fluid rounded-top" src="<?php echo base_url(); ?>assets/img/upload/<?= $buku->image; ?>" alt="...">
        </a>
        <div class="card-body p-2 mt-2">
          <a class="fw-bold" href="<?= base_url('home/detailBuku/' . $buku->id); ?>" style="text-decoration: none;">
            <?= $buku->judul_buku; ?></a>
          <p class="pt-2 mb-1 card-text small text-muted"><i class="fas fa-user-circle"></i> <?= $buku->pengarang; ?></p>
          <p class="mb-1 card-text small text-muted"><i class="fas fa-calendar-alt"></i> <?= $buku->tahun_terbit; ?> - <?= $buku->penerbit; ?></p>
        </div>
        <div class="card-footer p-0 mt-3 d-flex justify-content-between border-0 bg-white">

          <?php if ($buku->stok < 1) { ?>
            <div class="d-grid col-12">
              <a class="btn btn-danger btn-md"><i class="fas fa-shopping-cart"></i> Booking 0</a>
            </div>
          <?php } else { ?>
            <div class="d-grid col-12">
              <a class="btn btn-primary btn-md" href="<?= base_url('booking/tambahBooking/' . $buku->id); ?>">
              <i class="fas fa-shopping-cart"></i>&nbsp; Booking</a>
            </div>
          <?php } ?>

        </div>

      </div>
    </div>
<?php } ?>

</div>

</div>