<div class="container px-5">
	<div class="alert alert-info" role="alert">
		<i class="fas fa-check-circle"></i>
			<?php foreach ($useraktif as $u) { ?>
			Terima Kasih <strong><?= $u->nama; ?></strong>, Silahkan ambil buku anda dengan membawa bukti booking.
			<?php } ?>
	</div>

	<div class="card border-0 shadow-lg">

		<div class="card-header py-3 text-white bg-primary">
		<h6><i class="fas fa-book"></i> <?= $title; ?></h6>
		</div>

  		<div class="card-body mx-4">
  		
		<?php $no = 1; foreach ($items as $t) { ?>
			<div class="d-flex text-body-secondary pt-3">
	        <img src="<?= base_url('assets/img/upload/' . $t['image']); ?>" class="rounded" alt="..." style="max-height: 110px;">
	       
		        <div class="pb-3 mb-0 small lh-sm w-100">

		          <div class="d-flex justify-content-between">
		            <a class="fw-bold" style="text-decoration: none;">
		              <h6 class="ms-3"><?= $t['judul_buku']; ?></h6>
		            </a>
		          </div>

		          <span class="d-block ms-3">
		            <p class="mb-2"><i class="far fa-user-circle"></i> Penulis : &nbsp; <?= $t['pengarang']; ?></p>
		            <p class="mb-2"><i class="far fa-calendar-alt"></i> Tahun Terbit : &nbsp; <?= $t['tahun_terbit']; ?></p>
		            <p class="mb-2"><i class="fas fa-ribbon"></i> Penerbit : &nbsp; <?= $t['penerbit']; ?> </p>
		          </span>
		        </div>
	      	</div>
			<?php $no++; } ?>
  		</div>

  		<div class="card-footer bg-light py-3 text-center">

  			<a class="btn btn-success" href="<?php echo base_url() . 'booking/exportToPdf/' . $this->session->userdata('id_user'); ?>"><span class="far fa-lg fa-fw fa-file-pdf"></span> Cetak PDF</a>

	      <!-- <a class="btn btn-primary" href="<?php echo base_url() . 'booking/bookingSelesai/' . $this->session->userdata('id_user'); ?>">Selesai Booking <i class="far fa-arrow-alt-circle-right ms-1"></i></a> -->
  </div>

	</div>

</div>