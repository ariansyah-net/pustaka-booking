<style type="text/css">
.container {
	margin: 5px;
	padding: 10px;
	background-color: #fff;
	text-align: center;
	border-radius: 6px;
	box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
}
.footer {
	padding-top: 20px;
}
table{
	width: 100%;
	margin-left: auto;
	margin-right: auto;
	border: 1px solid #eee;
}
table th {
	padding: 10px;
	background-color: #19e3ea;
	margin: 0;
}
table tr, td {
	padding: 10px;
	text-align: left;
	border: none;
}

</style>

<div class="container">


	<?php foreach ($useraktif as $u) { ?>
	Nama : <strong><?= $u->nama; ?></strong>
	<p>Buku yang di booking:</p>
	<?php } ?>


	<table>
	<tr>
	<th>No.</th>
	<th>BUKU</th>
	<th>PENULIS</th>
	<th>PENERBIT</th>
	<th>TAHUN</th>
	</tr>
	<?php
	$no = 1;
	foreach ($items as $i) {
	?>
	<tr>
	<td style="text-align:center;"><?= $no; ?></td>
	<td>
	<?= $i['judul_buku']; ?>
	</td>
	<td><?= $i['pengarang']; ?></td>
	<td><?= $i['penerbit']; ?></td>
	<td style="text-align:center;"><?= $i['tahun_terbit']; ?></td>
	</tr>
	<?php $no++;
	} ?>
	</table>



	<div class="footer">
	<?php
	$path = 'assets/img/barcode.png';
	$type = pathinfo($path, PATHINFO_EXTENSION);
	$data = file_get_contents($path);
	$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
	?>
	<img src="<?php echo $base64?>" width="260" height="50"/>
	<p>
	<?= md5(date('d M Y H:i:s')); ?>
	</p>
	</div>

</div>