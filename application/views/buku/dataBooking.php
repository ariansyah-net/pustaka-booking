<div class="container px-5">
  
  <?= $this->session->flashdata('message'); ?>

<div class="card border-0 shadow-lg">


  <div class="card-header py-3 text-center text-white bg-secondary">
    <h6><i class="fas fa-shopping-cart"></i> <?= $title; ?></h6>
  </div>

  <div class="card-body mx-4">

    <?php $no = 1; foreach ($temp as $t) { ?>

      <div class="d-flex text-body-secondary pt-3">
        <a href="<?=base_url('home/detailBuku/'.$t['id_buku']);?>">
        <img src="<?= base_url('assets/img/upload/' . $t['image']); ?>" class="rounded" alt="..." style="max-height: 110px;">
        </a>
        <div class="pb-3 mb-0 small lh-sm w-100">

          <div class="d-flex justify-content-between">
            <a class="fw-bold" href="<?=base_url('home/detailBuku/'.$t['id_buku']);?>" style="text-decoration: none;">
              <h6 class="ms-3"><?= $t['judul_buku']; ?></h6>
            </a>
            <a class="btn btn-outline-danger" href="<?= base_url('booking/removeBooking/' . $t['id_buku']); ?>" onclick="return_konfirm('Ngga jadi booking buku ini? '.$t['judul_buku'] )">
            <i class="fas fa-trash-alt"></i>
            </a>
          </div>

          <span class="d-block ms-3">
            <p class="mb-2"><i class="far fa-user-circle"></i> Penulis : &nbsp; <?= $t['penulis']; ?></p>
            <p class="mb-2"><i class="far fa-calendar-alt"></i> Tahun Terbit : &nbsp; <?= $t['tahun_terbit']; ?></p>
            <p class="mb-2"><i class="fas fa-ribbon"></i> Penerbit : &nbsp; <?= $t['penerbit']; ?> </p>
          </span>
        </div>
      </div>
    <?php $no++; } ?>

  </div>

  <div class="card-footer bg-light py-3 d-flex justify-content-between">
    <a class="btn btn-success" href="<?php echo base_url(); ?>">
      <i class="far fa-arrow-alt-circle-left me-1"></i> Booking Lagi </a>

      <a class="btn btn-primary" href="<?php echo base_url() . 'booking/bookingSelesai/' . $this->session->userdata('id_user'); ?>">
        Selesai Booking <i class="far fa-arrow-alt-circle-right ms-1"></i></a>
  </div>


</div>

</div>