<header>
    <div class="px-3 py-2 text-bg-white">
      <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
          <a href="<?=base_url();?>" class="d-flex align-items-center my-2 my-lg-0 me-lg-auto text-dark text-decoration-none">
          	<img class="d-block mx-auto mb-1" src="<?= base_url('assets/img/sunrise.svg'); ?>">
        	&nbsp; <span class="fw-bold ms-2 lead">PUSTAKA BOOKING</span>
          </a>

          <ul class="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
            <li>
              <a href="<?= base_url(); ?>" class="nav-link text-dark">
                <img class="d-block mx-auto mb-1" src="<?= base_url('assets/img/home.svg'); ?>" style="width: 24; height: 24;">
                Beranda
              </a>
            </li>

            <li>
              <a href="<?=base_url(); ?>" class="nav-link text-dark">
                <img class="d-block mx-auto mb-1" src="<?= base_url('assets/img/grid.svg'); ?>" style="width: 24; height: 24;">
                Buku Terbaru
              </a>
            </li>
            <li>
              <a href="<?= base_url('booking'); ?>" class="nav-link text-dark">
                <img class="d-block mx-auto mb-1" src="<?= base_url('assets/img/table.svg'); ?>" style="width: 24; height: 24;">
                Booking 
                <span class="badge text-bg-primary">
                  <?= $this->ModelBooking->getDataWhere('temp', ['email_user' => $this->session->userdata('email')])->num_rows(); ?>
                </span>
              </a>
            </li>
              
	          	<?php if (!empty($this->session->userdata('email'))) { ?>
	          		<li>
	              		<a href="<?= base_url('member/profile');?>" class="nav-link text-primary">	
		                <img class="d-block mx-auto mb-1" src="<?= base_url('assets/img/users.svg'); ?>" style="width: 24; height: 24;">
		                <?php $nama; $split = explode(" ", $nama); echo $split[0]; ?>
		                </a>
	            	</li>
	            	<li class="border-start">
	              		<a href="<?= base_url('member/logout');?>" class="nav-link text-danger">	
		                <img class="d-block mx-auto mb-1" src="<?= base_url('assets/img/logout.svg'); ?>" style="width: 24; height: 24;">
		               	logout
		                </a>
	            	</li>
	          	<?php } else { ?>
	          		<li>
	              		<a href="#" data-bs-toggle="modal" data-bs-target="#loginModal" class="nav-link text-dark">
		                <img class="d-block mx-auto mb-1" src="<?= base_url('assets/img/login.svg'); ?>" style="width: 24; height: 24;">
	              		Login
	              		</a>
	          		</li>
	      		<?php } ?>
              
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="px-3 py-2 border-bottom mb-3"></div>
  </header>