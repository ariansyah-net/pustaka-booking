</div>

<div class="container sticky-footer pt-5">
  <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
    <p class="col-md-4 mb-0 text-body-secondary">© <?php echo date('Y'); ?> Ariansyah | Powered by Web Programming</p>

    <a href="#" class="col-md-4 d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-body-emphasis text-decoration-none">
    	<img class="img-fluid" src="<?= base_url('assets/img/sunrise.svg'); ?>">
    </a>

    <ul class="nav col-md-4 justify-content-end">
      <li class="nav-item"><a href="<?=base_url();?>" class="nav-link px-2 text-body-secondary">Beranda</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-body-secondary">FAQs</a></li>
      <li class="nav-item"><a href="<?=base_url('home/about');?>" class="nav-link px-2 text-body-secondary">Tentang</a></li>
      <li class="nav-item"><a href="<?=base_url('booking/info');?> " class="nav-link px-2 text-body-secondary">Info Booking</a></li>
    </ul>
  </footer>
</div>


<script src="<?= base_url('assets/'); ?>js/popper.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>
<script>
  $(document).ready(function() {
      $(".alert").fadeTo(2000, 700).slideUp(700, function() {
      $(".alert").slideUp(700);
    });
  });
</script>



</body>
</html>