 <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-6 col-lg-12 col-md-9">

                <div class="card border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">

                            <div class="col-lg-12 px-5">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Please Login</h1>
                                    </div>

                                    
<?= $this->session->flashdata('message'); ?>

<form class="user" method="post" action="<?= base_url('auth'); ?>">

<!-- Email -->
<div class="form-group">
<?= form_input('email', set_value('email'), array('class' => 'form-control form-control-user', 'placeholder' => 'Email')) ?>
<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
</div>

<!-- Password -->
<div class="form-group">
<!-- <input type="password" id="password" name="password" class="form-control form-control-user" placeholder="Password"> -->
<?= form_password('password', '', array('class' => 'form-control form-control-user', 'placeholder' => 'Password')) ?>
<?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
</div>

<!-- Remember Me -->
<div class="form-group">
<div class="custom-control custom-checkbox small">
<input type="checkbox" class="custom-control-input" id="customCheck">
<label class="custom-control-label" for="customCheck">Remember Me</label>
</div>
</div>
<button type="submit" class="btn btn-primary btn-user btn-block">Login</button>
</form>

<hr>
<div class="text-center">
<a class="small" href="<?= base_url('auth/forgot'); ?>">Forgot Password?</a>
</div>

<div class="text-center">
<a class="small" href="<?= base_url('auth/register'); ?>">Create an Account!</a>
</div>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

   