<div class="container">
<div class="card o-hidden border-0 shadow-lg my-5 col-lg-7 mx-auto">
<div class="card-body p-0">
<div class="row">

	<div class="col-lg">
	<div class="p-5">
		<div class="text-center"> <h1 class="h4 text-gray-900 mb-4">Reset Password</h1></div>
		<p class="text-gray-500 text-center small">
			Kami mengerti, banyak hal mungkin terjadi, cukup masukan email anda, sistem secara otomatis akan mengirimkan link untuk mereset password anda.
		</p>

		<form class="user" method="post" action="<?= base_url('auth/forgot'); ?>">
			
			<div class="form-group">
			<input type="text" class="form-control form-control-user" id="email" name="email" placeholder="Email" value="<?= set_value('email'); ?>">
			<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
			</div>

			<button type="submit" class="btn btn-primary btn-user btn-block"> Sent Password</button>
		</form>

		<hr>
		
		<div class="small text-center"> Kembali ke halaman <a class="" href="<?= base_url('auth'); ?>"> Login</a> </div>

	</div>
	</div>

</div>
</div>
</div>
</div>