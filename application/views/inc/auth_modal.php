
<div class="modal fade" tabindex="-1" id="loginModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Member Login</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      
      	<div class="modal-body">

	      	<form action="<?= base_url('member'); ?>" method="post">
						<div class="row ms-2 mb-3">
							<div class="col-2"><label for="mail" class="col-form-label text-muted">Email</label></div>
							<div class="col-9 ms-2">
							<input type="email" id="mail" name="email" class="form-control" required>
							</div>
						</div>
						<div class="row ms-2 mb-3">
							<div class="col-2"><label for="password" class="col-form-label text-muted">Password</label></div>
							<div class="col-9 ms-2">
							<input type="password" id="password" name="password" class="form-control" required>
							</div>
						</div>

						<div class="row ms-2 mb-3">
							<div class="col-2"><label for="password" class="col-form-label text-muted"></label></div>
							<div class="col-9 ms-2 d-grid gap-2 mx-auto">
					        <button type="submit" class="btn btn-block btn-primary"><i class="fas fa-user"></i> Login </button>
							</div>
						</div>
			</form>
		</div>

		<div class="d-flex modal-footer">
			<p class="text-muted small">Belum punya account? silahkan &nbsp; <a data-bs-toggle="modal" data-bs-target="#registerModal" class="btn btn-sm btn-success btn-rounded" href="#"><i class="fas fa-user-plus px-1"></i> Daftar</a></p>
		</div>
			
    </div>
  </div>
</div>




<!-- Register -->

<div class="modal fade" tabindex="-1" id="registerModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Registrasi Anggota</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

  		<form action="<?= base_url('member/registrasi'); ?>" method="post">

      	<div class="modal-body">
						<div class="mb-2">
							<label class="form-label text-muted">Nama</label>
							<input type="text" id="nama" name="nama" class="form-control" required>
						</div>
						<div class="mb-2">
							<label for="alamat" class="form-label text-muted">Alamat</label>
							<textarea id="alamat" name="alamat" class="form-control" required></textarea>
						</div>

						<div class="mb-2">
							<label for="mail" class="form-label text-muted">Email</label>
							<input type="email" id="mail" name="email" class="form-control" required>
						</div>
						<div class="mb-2">
							<label for="password1" class="col-form-label text-muted">Password</label>
							<input type="password" id="password1" name="password1" class="form-control" required>
						</div>

						<div class="mb-2">
							<label for="password2" class="col-form-label text-muted">Ulangi Password</label>
							<input type="password" id="password2" name="password2" class="form-control" required>
						</div>
	      </div>

	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary"><i class="fas fa-user"></i> Daftar </button>
	      </div>
      </form>
    </div>
  </div>
</div>


