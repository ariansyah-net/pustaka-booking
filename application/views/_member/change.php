<div class="container py-3">
<?php if(validation_errors()){?>
<div class="alert alert-danger" role="alert"><?= validation_errors();?></div>
<?php }?>

<a class="btn btn-secondary" onclick="history.back()">
  <i class="fas fa-arrow-left"></i></a>

<div class="row col-6 mx-auto bg-white rounded px-2 py-4 shadow">


<?= form_open_multipart('member/change'); ?>

  <div class="row mb-3">
  <label for="mail" class="col-sm-2 col-form-label">Email</label>
  <div class="col-sm-10">
  <input type="email" class="form-control text-muted" id="mail" name="email" value="<?= $email; ?>">
  </div>
  </div>

  <div class="row mb-3">
  <label for="name" class="col-sm-2 col-form-label">Nama</label>
  <div class="col-sm-10">
  <input type="name" class="form-control text-muted" id="name" name="nama" value="<?= $nama; ?>">
  </div>
  </div>

  <div class="row mb-3">
  <label for="addr" class="col-sm-2 col-form-label">Alamat</label>
  <div class="col-sm-10">
  <textarea class="form-control text-muted" id="addr" name="alamat" rows="3"><?= $alamat; ?></textarea>
  </div>
  </div>

  <div class="row mb-3">
  <label for="formFile" class="col-sm-2 col-form-label">Photo</label>
  <div class="col-sm-10">
  <input class="form-control text-muted" type="file" id="formFile" name="image">
  </div>
  </div>

  <div class="row mb-3">
  <label for="profil" class="col-sm-2 col-form-label">&nbsp;</label>
  <div class="col-sm-4 d-grid justify-content-end">
  <img class="img-fluid rounded" src="<?= base_url('assets/img/profile/') . $image; ?>" alt="...">
  </div>
  </div>


  <div class="row mb-3 pt-4">
  <div class="col-6 ms-3 d-grid justify-content-end">
  <button type="submit" class="btn btn-primary px-lg-5"><i class="fas fa-save"></i> Simpan</button>
  </div>
  </div>

    
<?= form_close(); ?>

</div>
</div>

