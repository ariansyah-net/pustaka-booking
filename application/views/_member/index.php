<div class="container">
	<?= $this->session->flashdata('message'); ?>
	
	<div class="card mb-3" style="width: 38rem;">
	<div class="row no-gutters">
		<div class="col-md-4">
			<img src="<?= base_url('assets/img/profile/') . $image; ?>" class="card-img" alt="...">
		</div>

		<div class="col-md-8">

			<div class="card-body">
				<h5 class="card-title"><?= $nama; ?></h5>
				<p class="card-text text-muted"><i class="fas fa-envelope"></i> &nbsp; <?= $email; ?></p>
				<p class="card-text text-muted"><i class="fas fa-home"></i> <?= $alamat; ?></p>
				<p class="card-text text-muted"><i class="fas fa-user-tag"></i>
				Member sejak: <span class="text-success"><?= date('d F Y', $tanggal_input); ?></span>
				</p>
				<a class="btn btn-secondary" href="<?= base_url('member/change'); ?>"><i class="fas fa-user-edit"></i> Ubah Profil</a>
			</div>
		</div>
	</div>
</div>

<!-- hal 99 -->