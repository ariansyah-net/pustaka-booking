<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
<a target="_blank" class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=base_url(); ?>">
<div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-mail-bulk"></i></div>
<div class="sidebar-brand-text mx-3">Booking Pustaka</div>
</a>

<hr class="sidebar-divider">

<div class="sidebar-heading">Master Data</div>
	<li class="nav-item">
		<li class="nav-item">
		<a class="nav-link pb-3" href="<?= base_url('admin'); ?>"><i class="fas fa-tachometer-alt"></i> <span class="ml-1">Dashboard</span></a>
		<hr class="pb-0 mb-0 sidebar-divider">
		</li>
		<li class="nav-item">
		<a class="nav-link pb-0" href="<?= base_url('buku'); ?>"><i class="fas fa-book"></i> <span class="ml-2">Data Buku</span></a>
		</li>
		<li class="nav-item">
		<a class="nav-link pb-0" href="<?= base_url('user/anggota'); ?>"><i class="fas fa-users"></i> <span class="ml-1">Data Anggota</span></a>
		</li>
	</li>


	<hr class="sidebar-divider mt-3">

	<div class="sidebar-heading">Transaksi</div>
	<li class="nav-item active">
		<li class="nav-item">
		<a class="nav-link pb-0" href="<?= base_url('pinjam'); ?>">
		<i class="fas fa-shopping-cart"></i><span class="ml-2">Data Peminjam</span></a>
		</li>
		<li class="nav-item">
		<a class="nav-link pb-0" href="<?= base_url('pinjam/daftarBooking'); ?>">
		<i class="fas fa-list-alt"></i><span class="ml-2">Data Booking</span></a>
		</li>
	</li>


	<hr class="sidebar-divider mt-3">

	<div class="sidebar-heading">Laporan</div>
	<li class="nav-item active">
		<li class="nav-item">
		<a class="nav-link pb-0" href="<?= base_url('laporan/buku'); ?>">
		<i class="fas fa-address-book"></i><span class="ml-2">Laporan Data Buku</span></a>
		</li>
		<li class="nav-item">
		<a class="nav-link pb-0" href="<?= base_url('laporan/anggota'); ?>">
		<i class="fas fa-address-book"></i><span class="ml-2">Laporan Data Anggota</span></a>
		</li>
		<li class="nav-item">
		<a class="nav-link pb-0" href="<?= base_url('laporan/pinjam'); ?>">
		<i class="fas fa-address-book"></i><span class="ml-2">Laporan Peminjaman</span></a>
		</li>
	</li>

	<hr class="sidebar-divider mt-3">

	<div class="text-center d-none d-md-inline">
	<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>
