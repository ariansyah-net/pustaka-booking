<div class="container-fluid">
<div class="row">

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                        Jumlah Anggota</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                        <?= $this->ModelUser->getUserWhere(['role_id' => 2])->num_rows(); ?>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                        Stok Buku Terdaftar</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                        <?php $where = ['stok != 0']; $totalstok = $this->ModelBuku->total('stok', $where); echo $totalstok; ?>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-book fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                        Buku yang dipinjam</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                        <?php $where = ['dipinjam != 0']; $totaldipinjam = $this->ModelBuku->total('dipinjam', $where); echo $totaldipinjam; ?>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-user-tag fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


   <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                        Buku yang dibooking</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                        <?php $where = ['dibooking !=0']; $totaldibooking = $this->ModelBuku->total('dibooking', $where); echo $totaldibooking; ?>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

 
</div><!-- /.row -->
</div><!-- /.container-fluid -->
</div><!-- End of Main Content -->