<div class="container-fluid">
<div class="row">
<div class="col-lg-12">

	<div class="d-flex justify-content-between mb-4">
		<?php foreach ($agt_booking as $ab) { ?>
		<div>Data Anggota : <strong><i class="fas fa-user-circle"></i> <?= $ab['nama']; ?></strong></div>
		<div>ID Booking : <strong><i class="fas fa-list-alt"></i> <?= $ab['id_booking']; ?></strong></div>
		<?php } ?>
	</div>
	
<div class="card px-4 py-4">
	<div class="table-responsive">
		<table class="table">
			<tr>
			<th>No.</th>
			<th>ID Buku</th>
			<th>Judul Buku</th>
			<th>Pengarang</th>
			<th>Penerbit</th>
			<th>Tahun</th>
			</tr>
			<?php $no = 1; foreach ($detail as $d) { ?>

			<tr>
			<td><?= $no; ?></td>
			<td><?= $d['id_buku']; ?></td>
			<td><?= $d['judul_buku']; ?></td>
			<td><?= $d['pengarang']; ?></td>
			<td><?= $d['penerbit']; ?></td>
			<td><?= $d['tahun_terbit']; ?></td>
			</tr>
			<?php $no++;
			} ?>
		</table>
	</div>


</div>

<div class="my-4">
<a href="#" onclick="window.history.go(-1)" class="btn btn-outline-dark">
<i class="fas fa-fw fa-reply"></i> Kembali</a>
</div>


</div>
</div>
</div>


</div><!--end main-->