<div class="container-fluid">
<div class="row">
<div class="col-lg-12">

<div class="d-flex justify-content-end mb-4">
<a href="<?= base_url('pinjam/daftarBooking'); ?>" class="btn btn-outline-primary">
<i class="fas fa-rocket"></i> Refresh</a></td>
</div>

<div class="card px-4 py-4">
	<div class="table-responsive">
		<table class="table">
			<tr>
			<th>No.</th>
			<th>ID Booking</th>
			<th>Tanggal Booking</th>
			<th>ID User</th>
			
			<th>Denda / Buku /Hari</th>
			<th>Lama Pinjam</th>
			<th>Aksi</th>
			</tr>

			<?php $no = 1; foreach ($pinjam as $p) { ?>
			<tr>
			<td><?= $no; ?></td>
			<td><a class="btn btn-outline-info" href="<?= base_url('pinjam/bookingDetail/' . $p['id_booking']); ?>"><?= $p['id_booking']; ?></a></td>
			<td><?= $p['tgl_booking']; ?></td>
			<td><?= $p['id_user']; ?></td>
			<form action="<?= base_url('pinjam/pinjamAct/' . $p['id_booking']); ?>" method="post">
			
			<td>
			<input class="form-control rounded-sm" style="width:100px" type="text" name="denda" id="denda" value="5000">
			<?= form_error(); ?>
			</td>
			<td>
			<input class="form-control rounded-sm" style="width:100px" type="text" name="lama" id="lama" value="3">
			<?= form_error(); ?>
			</td>
			<td nowrap>
			<button type="submit" class="btn btn-primary"><i class="fas fa-fw fa-cart-plus"></i> Pinjam</button>
			</td>

			</form>
			</tr>
			<?php $no++; } ?>
		</table>
	</div>

</div> <!--end card-->

</div>
</div>
</div>


</div><!--end main-->