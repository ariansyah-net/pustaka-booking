<div class="container-fluid">
<?= $this->session->flashdata('message'); ?>
<div class="row">
<div class="col-lg-12">

	<?php if(validation_errors()){?>
	<div class="alert alert-danger" role="alert">
	<?= validation_errors();?>
	</div>
	<?php }?>
	<?= $this->session->flashdata('message'); ?>

<a href="<?= base_url('laporan/pinjamCetak'); ?>" class="btn btn-outline-info mb-3"><i class="fas fa-print"></i> Print </a>
<a href="<?= base_url('laporan/pinjamPDF'); ?>" class="btn btn-outline-primary mb-3"><i class="far fa-file-pdf"></i> PDF</a>
<a href="<?= base_url('laporan/pinjamExcel'); ?>" class="btn btn-outline-success mb-3"><i class="far fa-file-excel"></i> Excel</a>

	<div class="card px-4 py-4">
	<div class="table-responsive">
	<table class="table table-hover">
		<thead>
		<tr>
		<th scope="col">#</th>
		<th scope="col">Nama Anggota</th>
		<th scope="col">Judul Buku</th>
		<th scope="col">Tanggal Pinjam</th>
		<th scope="col">Tanggal Kembali </th>
		<th scope="col">Tanggal Dikembalikan</th>
		<th scope="col">Total Denda</th>
		<th scope="col">Status</th>
		</tr>
		</thead>
		<tbody>
		<?php $a = 1; foreach ($laporan as $l) { ?>
		<tr>
		<th scope="row"><?= $a++; ?></th>
		<td><?= $l['nama']; ?></td>
		<td><?= $l['judul_buku']; ?></td>
		<td><?= $l['tgl_pinjam']; ?></td>
		<td><?= $l['tgl_kembali']; ?></td>
		<td><?= $l['tgl_pengembalian']; ?></td>
		<td><?= $l['total_denda']; ?></td>
		<td><?= $l['status']; ?></td>
		</tr>
		<?php } ?>
		</tbody>
	</table>
	</div>
	</div>


</div>
</div>
</div>


</div><!-- End of Main Content -->