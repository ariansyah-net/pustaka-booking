<div class="container-fluid">
<?= $this->session->flashdata('message'); ?>
<div class="row">
<div class="col-lg-12">


	<?php if(validation_errors()) { ?>
	<div class="alert alert-danger" role="alert">
	<?= validation_errors();?>
	</div>
	<?php }?>
	<?= $this->session->flashdata('message'); ?>

<a href="<?= base_url('laporan/bukuCetak'); ?>" class="btn btn-outline-info mb-3"><i class="fas fa-print"></i> Print</a>
<a href="<?= base_url('laporan/bukuPDF'); ?>" class="btn btn-outline-primary mb-3"><i class="far fa-file-pdf"></i> PDF</a>
<a href="<?= base_url('laporan/bukuExcel'); ?>" class="btn btn-outline-success mb-3"><i class="far fa-file-excel"></i> Excel</a>

	<div class="card px-4 py-4">
	<div class="table-responsive">
	<table class="table table-hover">
		<thead>
		<tr>
		<th scope="col">#</th>
		<th scope="col">Judul</th>
		<th scope="col">Pengarang</th>
		<th scope="col">Penerbit</th>
		<th scope="col">Tahun Terbit</th>
		<th scope="col">ISBN</th>
		<th scope="col">Stok</th>
		</tr>
		</thead>
		<tbody>
		<?php $a = 1; foreach ($buku as $b) { ?>
		<tr>
		<th scope="row"><?= $a++; ?></th>
		<td><?= $b['judul_buku']; ?></td>
		<td><?= $b['pengarang']; ?></td>
		<td><?= $b['penerbit']; ?></td>
		<td><?= $b['tahun_terbit']; ?></td>
		<td><?= $b['isbn']; ?></td>
		<td><?= $b['stok']; ?></td>
		</tr>
		<?php } ?>
		</tbody>
	</table>
	</div>
	</div>

</div>
</div>
</div>



</div>
