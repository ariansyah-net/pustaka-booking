<div class="container-fluid">

<div class="row">
<div class="col-lg-9">

	<?= form_open_multipart('buku/ubahBuku'); ?>

	<input type="hidden" name="id_buku" value="<?= $buku['id'];?>">

		<div class="form-group row">
			<label for="judul_buku" class="col-sm-2 col-form-label">Judul Buku</label>
			<div class="col-sm-10">
			<input type="text" class="form-control" id="judul_buku" name="judul_buku" value="<?= $buku['judul_buku']; ?>">
			</div>
		</div>

		<div class="form-group row">
			<label for="kategori" class="col-sm-2 col-form-label">Kategori</label>
			<div class="col-sm-8">
				<select name="id_kategori" class="form-control">
					<option class="pb-2" value="<?= $id; ?>"><?= $k; ?></option>
					<option></option>
					<?php foreach ($kategori as $kk) { ?>
						<option value="<?= $kk['id'];?>"><?= $kk['kategori'];?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="form-group row">
			<label for="pengarang" class="col-sm-2 col-form-label">Pengarang</label>
			<div class="col-sm-8">
			<input type="text" class="form-control" id="pengarang" name="pengarang" value="<?= $buku['pengarang']; ?>">
			</div>
		</div>

		<div class="form-group row">
			<label for="penerbit" class="col-sm-2 col-form-label">Penerbit</label>
			<div class="col-sm-8">
			<input type="text" class="form-control" id="penerbit" name="penerbit" value="<?= $buku['penerbit']; ?>">
			</div>
		</div>

		<div class="form-group row">
			<label for="tahun" class="col-sm-2 col-form-label">Tahun</label>
			<div class="col-sm-8">
				<select name="tahun_terbit" class="form-control">
					<option value="<?= $buku['tahun_terbit']; ?>" selected><?= $buku['tahun_terbit']; ?></option>
					
					<option value=""></option>
					<?php for ($i=date('Y'); $i > 2010 ; $i--) { ?>
					<option value="<?= $i;?>"><?= $i;?></option>
					<?php } ?>
				</select>
			</div>
		</div>


		<div class="form-group row">
			<label for="isbn" class="col-sm-2 col-form-label">ISBN</label>
			<div class="col-sm-8">
			<input type="text" class="form-control" id="isbn" name="isbn" value="<?= $buku['isbn']; ?>">
			</div>
		</div>

		<div class="form-group row">
			<label for="stok" class="col-sm-2 col-form-label">Stok</label>
			<div class="col-sm-3">
			<input type="text" class="form-control" id="stok" name="stok" value="<?= $buku['stok']; ?>">
			</div>
		</div>

	<div class="form-group row">

		<div class="col-sm-2">Sampul Buku</div>
		<div class="col-sm-10">
			<div class="row">
				<div class="col-sm-3">
				<img src="<?= base_url('assets/img/upload/') . $buku['image']; ?>" class="img-thumbnail" alt="...">
				</div>

				<div class="col-sm-9">
				<div class="custom-file">
				<input type="hidden" class="form-control" name="old_pict" value="<?= $buku['image']; ?>">
				<input type="file" class="custom-file-input" id="image" name="image">
				<label class="custom-file-label" for="image">Pilih file</label>
				</div>
				</div>
			</div>
		</div>
		</div>

		<div class="form-group row justify-content-end">
			<div class="col-sm-10">
			<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
			<!-- <button class="btn btn-dark" onclick="window.history.go(-1)"> Kembali</button> -->
			</div>
		</div>
	</form>

</div>
</div>
</div>


</div> <!--end main-->

