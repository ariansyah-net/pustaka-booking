<div class="container-fluid">
<?= $this->session->flashdata('message'); ?>
<div class="row">
<div class="col-12">

	<?php if(validation_errors()){?>
		<div class="alert alert-danger" role="alert"><?= validation_errors();?></div>
		<?php }?>

	<?= form_open('buku/ubahKategori'); ?>

		<input type="hidden" name="id_kategori" value="<?= $kategori['id'];?>">

		<div class="form-group row">
			<label for="kategori" class="col-sm-2 col-form-label">Kategori Buku</label>
			<div class="col-sm-8">
				<div class="form-group">
				<input type="text" class="form-control" name="kategori" value="<?= $kategori['kategori']; ?>" required>
			</div>
			</div>
		</div>

			<div class="form-group row">
				
				<div class="col-sm-2">
				<button type="submit" class="btn btn-secondary btn-sm"><i class="fas fa-arrow-left"></i></button>
				</div>

				<div class="col-sm-8">
				<button type="submit" class="btn btn-primary">Ubah</button>
				</div>
			</div>
	<?= form_close()?>

</div>
</div>
</div>


</div> <!--end main-->

