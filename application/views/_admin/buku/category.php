<div class="container-fluid">


<?= $this->session->flashdata('message'); ?>

<div class="row">
<div class="col-lg-12">

	<a class="btn btn-primary mb-3" data-toggle="modal" data-target="#kategoriBaruModal">
		<i class="fas fa-file-alt"></i> Tambah</a>

	<div class="card px-4 py-4">
	<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">No.</th>
				<th scope="col">Kategori</th>
				<th scope="col">Pilihan</th>
			</tr>
		</thead>

		<tbody>
		<?php $a = 1; foreach ($kategori as $k) { ?>
		<tr>
			<th scope="row"><?= $a++; ?></th>
			<td><?= $k['kategori']; ?></td>
			<td>
				<a href="<?= base_url('buku/ubahKategori/').$k['id'];?>" class="btn btn-sm btn-rounded btn-info">
				<i class="far fa-edit"></i></a> &nbsp;
				<a href="<?= base_url('buku/hapusKategori/').$k['id'];?>" onclick="return confirm('Are you sure to remove <?= $title.' '.$k['kategori'];?> ?');" class="btn btn-sm btn-danger">
				<i class="fas fa-trash-alt"></i></a>
			</td>
		</tr>
		<?php } ?>
		</tbody>
	</table>
	</div>
	</div>


</div>
</div>
</div>

</div><!-- End of Main Content -->




<!-- Modal Category-->
	<div class="modal fade" id="kategoriBaruModal" tabindex="-1" role="dialog" aria-labelledby="newCategory" aria-hidden="true">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		
		<div class="modal-header">
		<h5 class="modal-title" id="newCategory">Tambah Kategori Buku</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
		</div>

		<form action="<?= base_url('buku/kategori'); ?>" method="post">

			<div class="modal-body">
			<div class="form-group">
				<label class="form-label">Kategori</label>
				<input type="text" class="form-control" name="kategori">
			</div>
			</div>

			<div class="modal-footer">
			<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
			</div>

		</form>


	</div>
	</div>
	</div>
