<div class="container-fluid"><?= $this->session->flashdata('message'); ?>
<div class="row">
<div class="col-lg-12">

<div class="d-flex justify-content-between">
	<button class="btn btn-primary mb-3" data-toggle="modal" data-target="#bukuBaruModal">
	<i class="fas fa-file-alt"></i> Buku Baru
	</button>
	<a href="<?= base_url('buku/kategori'); ?>" class="btn btn-secondary mb-3">
		<i class="fas fa-list-alt"></i> Kategori Buku
	</a>
</div>

	<div class="card px-4 py-4">
		<?php if(validation_errors()){?>
		<div class="alert alert-danger" role="alert"><?= validation_errors();?></div>
		<?php }?>

		<div class="table-responsive">
		<table class="table table-hover">
			<thead>
			<tr>
				<th scope="col">No.</th>
				<th scope="col">Judul</th>
				<th scope="col">Pengarang</th>
				<th scope="col">Penerbit</th>
				<th scope="col">Tahun Terbit</th>
				<th scope="col">ISBN</th>
				<!-- <th scope="col">Stok</th> -->
				<!-- <th scope="col">DiPinjam</th> -->
				<!-- <th scope="col">DiBooking</th> -->
				<th scope="col">Cover</th>
				<th scope="col">Pilihan</th>
			</tr>
			</thead>
		<tbody>
			<?php $a = 1; foreach ($buku as $b) { ?>
			<tr>
				<th scope="row"><?= $a++; ?></th>
				<td><?= $b['judul_buku']; ?></td>
				<td><?= $b['pengarang']; ?></td>
				<td><?= $b['penerbit']; ?></td>
				<td><?= $b['tahun_terbit']; ?></td>
				<td><?= $b['isbn']; ?></td>
				<!-- <td><?= $b['stok']; ?></td> -->
				<!-- <td><?= $b['dipinjam']; ?></td> -->
				<!-- <td><?= $b['dibooking']; ?></td> -->
				<td>
				<img class="img-fluid rounded-lg" style="max-height: 64px;" src="<?= base_url('assets/img/upload/') . $b['image'];?>" alt="...">
				</td>
				<td>
				<a href="<?= base_url('buku/ubahBuku/').$b['id'];?>" class="btn btn-sm btn-info" title="Edit"><i class="far fa-edit"></i></a>
				<a href="<?= base_url('buku/hapusbuku/').$b['id'];?>" onclick="return confirm('Are you sure to remove <?= $title.' '.$b['judul_buku'];?> ?');" class="btn btn-sm btn-danger" title="Hapus"><i class="fas fa-trash-alt"></i></a>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		</table>
		</div>
	</div>

</div>
</div>
</div>


</div> <!--end main-->


<!-- Modal buku-->
<div class="modal fade" id="bukuBaruModal" tabindex="-1" role="dialog" aria-labelledby="bukuBaruModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="bukuBaruModalLabel">Tambah Buku</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>

	<?= form_open_multipart('buku'); ?>
		<div class="modal-body">

			<div class="row">
				<div class="col mb-3"><input type="text" class="form-control" name="judul_buku" placeholder="Judul Buku"></div>
				<div class="col mb-3">
					<select name="id_kategori" class="custom-select">
						<option value="">Kategori</option>
						<?php foreach ($kategori as $k) { ?>
						<option value="<?= $k['id'];?>"><?= $k['kategori'];?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="row">
				<div class="col mb-3"><input type="text" class="form-control" name="pengarang" placeholder="Nama Pengarang"></div>
				<div class="col mb-3"><input type="text" class="form-control" name="penerbit" placeholder="Penerbit"></div>
			</div>

			<div class="row">
				<div class="col mb-3">
					<select name="tahun_terbit" class="custom-select">
						<option value="">Tahun</option>
						<?php for ($i=date('Y'); $i > 2010 ; $i--) { ?>
						<option value="<?= $i;?>"><?= $i;?></option>
						<?php } ?>
					</select>

				</div>
				<div class="col mb-3"><input type="text" class="form-control" name="isbn" placeholder="ISBN"></div>
			</div>

			<div class="row">
				<div class="col-sm-6 mb-3"><input type="text" class="form-control" name="stok" placeholder="Stok Buku"></div>
				<div class="col mb-3 ml-2 mr-2">
					<input type="file" class="custom-file-input" id="image" name="image">
					<label class="custom-file-label" for="image">Sampul Buku..</label>
				</div>
			</div>
			
		</div> <!--end modal body-->
		<div class="modal-footer">
		<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
		</div>

	<?= form_close(); ?>
</div>
</div>
</div>
