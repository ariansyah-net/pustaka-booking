<div class="container-fluid">

	<div class="row">
	<div class="col-lg-6 justify-content-x">
	<?= $this->session->flashdata('message'); ?>
	</div>
	</div>
	
	<div class="card mb-3" style="max-width: 540px;">
		<div class="row no-gutters">
			<div class="col-md-4">
			<img src="<?= base_url('assets/img/profile/') . $user['image']; ?>" class="card-img" alt="...">
			</div>

			<div class="col-md-8">
				<div class="card-body text-center">
					<h5 class="card-title font-weight-bold"><?= $user['nama']; ?></h5>
					<p class="card-text mb-0"><?= $user['alamat']; ?></p>
					<p class="card-text mb-0"><?= $user['email']; ?></p>
					<p class="card-text">
						<span class="text-muted">
							Member sejak: <span class="text-success"><?= date('d F Y', $user['tanggal_input']); ?></span>
						</span>
					</p>
				
					<div class="my-3 btn-sm">
					<a href="<?= base_url('user/ubahprofil'); ?>" class="btn btn-secondary btn-sm"><i class="fas fa-user-edit"></i> Ubah Profil</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

</div>