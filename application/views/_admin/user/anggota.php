<div class="container-fluid">
	<?= $this->session->flashdata('message'); ?>
	<?php if(validation_errors()){?>
	<div class="alert alert-danger" role="alert"><?= validation_errors();?></div>
	<?php }?>
<div class="row">
<div class="col-lg-12">

<div class="d-flex justify-content-between">
	<button class="btn btn-primary mb-3" data-toggle="modal" data-target="#userModal">
	<i class="fas fa-user-plus"></i> Tambah
	</button>
</div>

	<div class="card px-4 py-4">
		
		<div class="table-responsive">
		<table class="table table-hover">
			<thead>
			<tr>
				<th scope="col">No.</th>
				<th scope="col">Nama</th>
				<th scope="col">Alamat</th>
				<th scope="col">Email</th>
				<th scope="col">Role</th>
				<th scope="col">Status</th>
				<th scope="col">Aksi</th>
			</tr>
			</thead>
		<tbody>
			<?php $i = 1; foreach ($anggota as $ar) { ?>
			<tr>
				<th scope="row"><?= $i++; ?></th>
				<td>
					<img class="img-fluid rounded-lg" style="max-width: 45px;" src="<?= base_url('assets/img/profile/') . $ar['image'];?>" alt="...">
					<span class="text-default"><?= $ar['nama']; ?></span>
				</td>
				<td><?= $ar['alamat']; ?></td>
				<td><?= $ar['email']; ?></td>
				<td>
					<?php if($ar['role_id'] == 1) { ?>
						<span class="badge badge-info"><i class="fas fa-user-edit"></i> Administrator</span>
					<?php } else { ?> 
						<span class="badge badge-light"><i class="fas fa-user-circle"></i> User</span>
					<?php } ?>
				</td>
				<td>
					<?php if($ar['is_active'] == 1) { ?>
						<span class="badge badge-light"><i class="fas fa-user-check"></i> Aktif</span>
					<?php } else { ?> 
						<span class="badge badge-info"><i class="fas fa-user-minus"></i> Tidak Aktif</span>
					<?php } ?>
					</td>
			
				<td>
				<a href="<?= base_url('user/change/').$ar['id'];?>" class="btn btn-sm btn-info" title="Edit"><i class="far fa-edit"></i></a> &nbsp;
				<a href="<?= base_url('user/hapusUser/').$ar['id'];?>" onclick="return confirm('Anda yakin akan menghapus <?= $ar['nama'];?> ?');" class="btn btn-sm btn-danger" title="Hapus"><i class="fas fa-trash-alt"></i></a>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		</table>
		</div>
	</div>

</div>
</div>
</div>


</div> <!--end main-->


<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">

	<div class="modal-header">
		<h5 class="modal-title" id="userModalLabel">Tambah Anggota</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
	</div>


	<?= form_open_multipart('user/anggota'); ?>
		<div class="modal-body">

			<div class="row">
				<div class="col mb-3"><input type="text" class="form-control" name="nama" placeholder="Nama"></div>
				<div class="col mb-3">
					<select name="role_id" class="custom-select">
						<option value="">---</option>
						<option value="2">Pengguna</option>
						<option value="1">Administrator</option>
					</select>
				</div>
			</div>

			<div class="row">
				<div class="col mb-3"><input type="text" class="form-control" name="email" placeholder="Email"></div>
				<div class="col mb-3"><input type="password" class="form-control" name="password" placeholder="Password"></div>
			</div>

			<div class="row">
				<div class="col-sm-6 mb-3"><textarea class="form-control" name="alamat" placeholder="Alamat"></textarea></div>
				<div class="col mb-3 ml-2 mr-2">
					<input type="file" class="custom-file-input" id="image" name="image">
					<label class="custom-file-label" for="image">Photo</label>
				</div>
			</div>
			
		</div> <!--end modal body-->

		<div class="modal-footer">
		<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
		</div>

	<?= form_close(); ?>


</div>
</div>
</div>

