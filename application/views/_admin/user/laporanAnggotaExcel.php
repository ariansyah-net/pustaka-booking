<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=$title.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
?>

<h3><center>Laporan Data Anggota Perputakaan Online</center></h3>
<br/>
<table class="table-data">
<thead>
<tr>
<th scope="col">No.</th>
<th scope="col">Nama</th>
<th scope="col">Alamat</th>
<th scope="col">Email</th>
<th scope="col">Role</th>
<th scope="col">Status</th>
</tr>
</thead>
<tbody>
<?php $no = 1; foreach($anggota as $ar) { ?>
	<tr>
		<td><?= $no++; ?></td>
		<td><?= $ar['nama']; ?></td>
		<td><?= $ar['alamat']; ?></td>
		<td><?= $ar['email']; ?></td>
		<td>
			<?php if($ar['role_id'] == 1) { ?>
			<span class="badge badge-info"><i class="fas fa-user-edit"></i> Administrator</span>
			<?php } else { ?> 
			<span class="badge badge-light"><i class="fas fa-user-circle"></i> User</span>
			<?php } ?>
		</td>
		<td>
			<?php if($ar['is_active'] == 1) { ?>
			<span class="badge badge-light"><i class="fas fa-user-check"></i> Aktif</span>
			<?php } else { ?> 
			<span class="badge badge-info"><i class="fas fa-user-minus"></i> Tidak Aktif</span>
			<?php } ?>
			</td>
	</tr>
	<?php } ?>
</tbody>
</table>
