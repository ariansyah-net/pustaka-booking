<div class="container-fluid">

<div class="row">
<div class="col-lg-9">

	<?= form_open_multipart('user/change'); ?>
		<?= form_hidden('user_id', $user['id']); ?>

		<div class="form-group row">
			<label for="email" class="col-sm-2 col-form-label">Email</label>
			<div class="col-sm-8">
			<input type="text" class="form-control" id="email" name="email" value="<?= $user['email']; ?>" readonly>
			</div>
		</div>

		<div class="form-group row">
			<label for="nama" class="col-sm-2 col-form-label">Nama Lengkap</label>
			<div class="col-sm-8">
			<input type="text" class="form-control" id="nama" name="nama" value="<?= $user['nama']; ?>">
			<?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
			</div>
		</div>

		<div class="form-group row">
			<label for="role_id" class="col-sm-2 col-form-label">Peran / Status</label>
			<div class="col-sm-4">
				<select name="role_id" class="custom-select">
					<?php if($user['role_id'] == 1) {
						echo "<option value='1' selected>Administrator</option>";
						echo "<option value='2'>User</option>";
					} else {
						echo "<option value='2' selected>User</option>";
						echo "<option value='1'>Administrator</option>";
					}
					?>
				</select>
			</div>
			
			<div class="col-sm-4">
				<select name="is_active" class="custom-select">
					<?php if($user['is_active'] == 1) {
						echo "<option value='1' selected>Aktif</option>";
						echo "<option value='0'>Tidak Aktif</option>";
					} else {
						echo "<option value='0' selected>Tidak Aktif</option>";
						echo "<option value='1'>Aktif</option>";
					}
					?>
				</select>
			</div>
		</div>

		<div class="form-group row">
			<label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
			<div class="col-sm-10">
			<textarea type="text" class="form-control" id="alamat" name="alamat"><?= $user['alamat']; ?></textarea>
			<?= form_error('alamat', '<small class="text-danger pl-3">', '</small>'); ?>
			</div>
		</div>


	<div class="form-group row">

		<div class="col-sm-2">Photo</div>
		<div class="col-sm-10">
			<div class="row">
				<div class="col-sm-3">
				<img src="<?= base_url('assets/img/profile/') . $user['image']; ?>" class="img-thumbnail">
				</div>

				<div class="col-sm-9">
				<div class="custom-file">
				<input type="file" class="custom-file-input" id="image" name="image" value="<?= $user['image']; ?>">
				<label class="custom-file-label" for="image">Pilih file</label>
				</div>
				</div>
			</div>
		</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-2">
			<a class="btn btn-secondary btn-sm" onclick="history.back()"><i class="fas fa-arrow-left"></i></a>
			</div>
			<div class="col-sm-10">
			<button type="submit" class="btn px-5 btn-primary"><i class="fas fa-save"></i> Simpan</button>
			</div>
		</div>
	</form>

</div>
</div>
</div>

</div> <!--end main-->