<div class="container-fluid">

<div class="row">
<div class="col-lg-12">

<a href="<?= base_url('laporan/anggotaCetak'); ?>" class="btn btn-outline-info mb-3"><i class="fas fa-print"></i> Print</a>
<a href="<?= base_url('laporan/anggotaPDF'); ?>" class="btn btn-outline-primary mb-3"><i class="far fa-file-pdf"></i> PDF</a>
<a href="<?= base_url('laporan/anggotaExcel'); ?>" class="btn btn-outline-success mb-3"><i class="far fa-file-excel"></i> Excel</a>

	<div class="card px-4 py-4">
		<div class="table-responsive">
		<table class="table table-hover">
			<thead>
			<tr>
				<th scope="col">No.</th>
				<th scope="col">Nama</th>
				<th scope="col">Alamat</th>
				<th scope="col">Email</th>
				<th scope="col">Role</th>
				<th scope="col">Status</th>
			</tr>
			</thead>
		<tbody>
			<?php $i = 1; foreach ($anggota as $ar) { ?>
			<tr>
				<th scope="row"><?= $i++; ?></th>
				<td><?= $ar['nama']; ?></td>
				<td><?= $ar['alamat']; ?></td>
				<td><?= $ar['email']; ?></td>
				<td>
					<?php if($ar['role_id'] == 1) { ?>
					<span class="badge badge-info"><i class="fas fa-user-edit"></i> Administrator</span>
					<?php } else { ?> 
					<span class="badge badge-light"><i class="fas fa-user-circle"></i> User</span>
					<?php } ?>
				</td>
				<td>
					<?php if($ar['is_active'] == 1) { ?>
					<span class="badge badge-light"><i class="fas fa-user-check"></i> Aktif</span>
					<?php } else { ?> 
					<span class="badge badge-info"><i class="fas fa-user-minus"></i> Tidak Aktif</span>
					<?php } ?>
					</td>
			</tr>
			<?php } ?>
		</tbody>
		</table>
		</div>
	</div>

</div>
</div>
</div>


</div> <!--end main-->