<!DOCTYPE html>
<html>
<head>
<title></title>
</head>
<body>
<style type="text/css">
	
.table-data{
width: 100%;
border-collapse: collapse;
}
.table-data tr th,
.table-data tr td{
border:1px solid black;
font-size: 11pt;
padding: 10px 10px 10px 10px;
}
h3{
font-family:Verdana;
}
</style>

<h3><center>Laporan Data Anggota Perputakaan Online</center></h3>
<br/>
<table class="table-data">
<thead>
<tr>
<th scope="col">No.</th>
<th scope="col">Nama</th>
<th scope="col">Alamat</th>
<th scope="col">Email</th>
<th scope="col">Role</th>
<th scope="col">Status</th>
</tr>
</thead>
<tbody>
<?php $no = 1; foreach($anggota as $ar) { ?>
	<tr>
		<th><?= $no++; ?></th>
		<td><?= $ar['nama']; ?></td>
		<td><?= $ar['alamat']; ?></td>
		<td><?= $ar['email']; ?></td>
		<td>
			<?php if($ar['role_id'] == 1) { ?>
			<span class="badge badge-info"><i class="fas fa-user-edit"></i> Administrator</span>
			<?php } else { ?> 
			<span class="badge badge-light"><i class="fas fa-user-circle"></i> User</span>
			<?php } ?>
		</td>
		<td>
			<?php if($ar['is_active'] == 1) { ?>
			<span class="badge badge-light"><i class="fas fa-user-check"></i> Aktif</span>
			<?php } else { ?> 
			<span class="badge badge-info"><i class="fas fa-user-minus"></i> Tidak Aktif</span>
			<?php } ?>
			</td>
	</tr>
	<?php } ?>

</tbody>
</table>
</body>
</html>