<?php class Auth extends CI_Controller {


	function index() {

		if($this->session->userdata('role_id' == 1)) { redirect('user');  }

		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', 
			[
				'required' 		=> 'Email nya diisi dulu om..',
				'valid_email' 	=> 'Emailnya ngga benar nih..'
			]);

		$this->form_validation->set_rules('password', 'Password', 'required|trim',
			[
				'required' => 'Password masa ngga diisi..'
			]);

		if ($this->form_validation->run() == false) {

			$data['title'] = 'Login';
			$data['user'] = '';
			$this->load->view('inc/auth_header', $data);
			$this->load->view('inc/login');
			$this->load->view('inc/auth_footer');
			return;
		} else {
			$this->_login();
		}
	}



	private function _login() {

		$email = htmlspecialchars($this->input->post('email', true));
		$password = $this->input->post('password', true);
		$user = $this->ModelUser->cekData(['email' => $email, 'role_id' => '1'])->row_array();
		
		if ($user) {

			if ($user['role_id'] == 1 && $user['is_active'] == 1) {

				if (password_verify($password, $user['password'])) {

					$data = [
						'email' 	=> $user['email'],
						'role_id' 	=> $user['role_id']
					];

					$this->session->set_userdata($data);

						if ($user['image'] == 'default.png') {
							$this->session->set_flashdata('message', '<div class="alert alert-info alert-message" role="alert">Silahkan ubah photo profil anda.</div>');
							redirect('user');
					 	} else {
					 		redirect('admin');
					 	}

				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger alert-message" role="alert">Passwordnya salah om.</div>');
					redirect('auth');
				}

			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-message" role="alert">Maaf, akun administrator anda belum diaktifkan.</div>'); 
				redirect('auth');
			}

		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-message" role="alert">Email isn`t registered.</div>');
			redirect('auth');
		}

	}


	function blok() {
		$this->load->view('inc/block');
	}

	function gagal() {
		$this->load->view('inc/gagal');
	}




	function register() {
		
		if ($this->session->userdata('role_id' == 1)) {
			redirect('user');
		}

		$this->form_validation->set_rules('nama', 'Nama Lengkap',
		'required', [
			'required' 	=> 'Nama belum di isi om.'
		]);

		$this->form_validation->set_rules('email', 'Alamat Email',
			'required|trim|valid_email|is_unique[user.email]', [
				'valid_email' 	=> 'Email nya ngga bener nih..',
				'required' 		=> 'Email belum di isi om.',
				'is_unique' 	=> 'Email ini udah terdaftar om.'
			]);

		$this->form_validation->set_rules('password1', 'Password',
			'required|trim|min_length[3]|matches[password2]', [
				'required' 		=> 'Password di isi dong om.',
				'matches' 		=> 'Passwordnya ngga sama om.',
				'min_length' 	=> 'Password Terlalu Pendek'
			]);

	$this->form_validation->set_rules('password2', 'Repeat Password',
		'required|trim|matches[password1]', [
			'required' 		=> 'Password ini juga di isi dong om.',
			'matches' 		=> 'Passwordnya ngga sama om.',
			'min_length'	=> 'Password Terlalu Pendek'
		]);

	if ($this->form_validation->run() == false) {

		$data['title'] = 'Member Registration';
		$this->load->view('inc/auth_header', $data);
		$this->load->view('inc/register');
		$this->load->view('inc/auth_footer');
	
	} else {
		$email = $this->input->post('email', true);
		$data = [
			'nama' 			=> htmlspecialchars($this->input->post('nama',true)),
			'email' 		=> htmlspecialchars($email),
			'image' 		=> 'default.png',
			'password' 		=> password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
			'role_id' 		=> 2,
			'is_active' 	=> 0,
			'tanggal_input' => time()
	];

		$this->ModelUser->simpanData($data);
		$this->session->set_flashdata('message', '<div class="alert alert-info alert-message" role="alert">Congratulations, your account has been successfully created, please activate your account via your email.</div>');
		redirect('auth');
		}
	}

	function forgot() {

		// ngopi dulu

		$data['title'] = 'Forgot Password?';
		$this->load->view('inc/auth_header', $data);
		$this->load->view('inc/forgot');
		$this->load->view('inc/auth_footer');
	}
	

	function logout() {
        $data = [
            'email'      => null,
            'role_id'    => null
        ];
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        redirect(base_url());
        $this->session->set_flashdata('message', '<div class="alert alert-info alert-message" role="alert">You have been logged out!</div>');
    }


}