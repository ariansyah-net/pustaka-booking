<?php class Home extends CI_Controller {

function __construct() {
	parent::__construct();
}

 	function index() {

		$data = [
			'title' => "Katalog Buku",
			'buku' => $this->ModelBuku->getBuku()->result(),
		];

		if ($this->session->userdata('email')) {
			$user = $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
			$data['nama'] = $user['nama'];
			$this->load->view('inc/header', $data);
			$this->load->view('inc/navbar', $data);
			$this->load->view('buku/index', $data);
			$this->load->view('inc/auth_modal');
			$this->load->view('inc/footer', $data);
		} else {
			$data['nama'] = 'Pengunjung';
			$this->load->view('inc/header', $data);
			$this->load->view('inc/navbar', $data);
			$this->load->view('buku/index', $data);
			$this->load->view('inc/auth_modal');
			$this->load->view('inc/footer', $data);
		}
	}


	function detailBuku() {

		$id = $this->uri->segment(3);
		$buku = $this->ModelBuku->joinKategoriBuku(['buku.id' => $id])->result();
		$data['title'] = "Detail Buku";
		
		foreach ($buku as $fields)
			{
				$data['judul'] = $fields->judul_buku;
				$data['pengarang'] = $fields->pengarang;
				$data['penerbit'] = $fields->penerbit;
				$data['kategori'] = $fields->kategori;
				$data['tahun'] = $fields->tahun_terbit;
				$data['isbn'] = $fields->isbn;
				$data['gambar'] = $fields->image;
				$data['dipinjam'] = $fields->dipinjam;
				$data['dibooking'] = $fields->dibooking;
				$data['stok'] = $fields->stok;
				$data['id'] = $id;
			}

		if ($this->session->userdata('email')) {
			$user = $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
			$data['nama'] = $user['nama'];
			$this->load->view('inc/header', $data);
			$this->load->view('inc/navbar', $data);
			$this->load->view('buku/detailBuku', $data);
			$this->load->view('inc/auth_modal');
			$this->load->view('inc/footer', $data);
		} else {
			$data['nama'] = 'Pengunjung';
			$this->load->view('inc/header', $data);
			$this->load->view('inc/navbar', $data);
			$this->load->view('buku/detailBuku', $data);
			$this->load->view('inc/auth_modal');
			$this->load->view('inc/footer', $data);
		}

	}


	function about() {
		$data['title'] 	= "Tentang Aplikasi";
		$user = $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
		
		if ($this->session->userdata('email')) {
			$data['nama'] 	= $user['nama'];
			$this->load->view('inc/header', $data);
			$this->load->view('inc/navbar', $data);
			$this->load->view('buku/about');
			$this->load->view('inc/auth_modal');
			$this->load->view('inc/footer', $data);
		} else {
			$data['nama'] 	= 'pengunjung';
			$this->load->view('inc/header', $data);
			$this->load->view('inc/navbar', $data);
			$this->load->view('buku/about');
			$this->load->view('inc/auth_modal');
			$this->load->view('inc/footer', $data);
		}

	}
}
