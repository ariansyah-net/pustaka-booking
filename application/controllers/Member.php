<?php class Member extends CI_Controller {

	function __construct() {
		parent::__construct();
		// cek_login();
	}

	function index() {
		$data['title'] = 'Profil Saya';
		$this->_login();
	}

	private function _login() {

		$email = htmlspecialchars($this->input->post('email', true));
		$password = $this->input->post('password', true);
		$user = $this->ModelUser->cekData(['email' => $email])->row_array();

		if ($user) {
			if ($user['role_id'] == 2 && $user['is_active'] == 1) {

				if (password_verify($password, $user['password'])) {
					$data = [
						'email' 	=> $user['email'],
						'role_id' 	=> $user['role_id'],
						'id_user' 	=> $user['id'],
						'nama' 		=> $user['nama']
						];
					$this->session->set_userdata($data);
					$this->session->set_flashdata('message',
						'<div class="alert alert-primary alert-dismissible fade show" role="alert">
						<i class="fas fa-user-circle"></i> Selamat datang <strong>'.$data['nama'].'</strong>
				  		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div>');

					redirect('home');
				} else {
					$this->session->set_flashdata('message',
						'<div class="alert alert-danger alert-dismissible fade show" role="alert">
						Passwordnya salah nih, silahkan coba lagi..
				  		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div>');
						redirect('home');
					}
			} else {
				$this->session->set_flashdata('message',
					'<div class="alert alert-danger alert-dismissible fade show" role="alert">
					User belum diaktifasi, atau belum diaktifkan oleh administrator.
				  	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>');
				redirect('home');
			}

		} else {
			$this->session->set_flashdata('message', 
				'<div class="alert alert-danger alert-dismissible fade show" role="alert">
				Maaf, email anda tidak terdaftar.
			  	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				</div>');
			redirect('home');
		}

	}




	function registrasi() {

		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|trim', ['required' => 'Nama Belum diisi!!']);
		$this->form_validation->set_rules('alamat', 'Alamat Lengkap', 'required|trim', ['required' => 'Alamat Belum diis!!']);
		$this->form_validation->set_rules('email', 'Alamat Email', 'required|trim|valid_email|is_unique[user.email]', [
			'valid_email' 	=> 'Email ngga bener nih.',
			'required' 		=> 'Emailnya di isi dong.',
			'is_unique' 	=> 'Sepertinya email ini sudah terdaftar, silahkan login'
			]);

		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', ['matches' => 'Password tidak sama!!', 'min_length' => 'Password terlalu pendek']);
		$this->form_validation->set_rules('password2', 'Repeat Password', 'required|trim|matches[password1]');
		$email = $this->input->post('email', true);
		$data = [
			'nama' 			=> htmlspecialchars($this->input->post('nama', true)),
			'alamat' 		=> $this->input->post('alamat', true),
			'email' 		=> htmlspecialchars($email),
			'image' 		=> 'default.png',
			'password' 		=> password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
			'role_id' 		=> 2,
			'is_active' 	=> 1,
			'tanggal_input' => time()
			];

		$this->ModelUser->simpanData($data);
		$this->session->set_flashdata('message',
			'<div class="alert alert-info alert-dismissible fade show" role="alert">
			<i class="fas fa-check-circle"></i> Selamat!! akun anda berhasil dibuat, silahkan lakukan login.
		  	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>');
		
		redirect(base_url());
	
	}

	function profile() {

		$user = $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
		foreach ($user as $a) {
			$data = [
					'image' 		=> $user['image'],
					'nama' 			=> $user['nama'],
					'email' 		=> $user['email'],
					'alamat'		=> $user['alamat'],
					'tanggal_input' => $user['tanggal_input'],
				];
		}
			$data['title'] = 'Profil Saya';
			$this->load->view('inc/header', $data);
			$this->load->view('inc/navbar', $data);
			$this->load->view('_member/index', $data);
			$this->load->view('inc/auth_modal');
			$this->load->view('inc/footer', $data);
		}

	function logout() {

		$this->session->unset_userdata('email');
		$this->session->unset_userdata('role_id');
		$this->session->set_flashdata('message', 
			'<div class="alert alert-info alert-dismissible fade show" role="alert">
			Anda telah keluar..
		  	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>');
		redirect('home');
	}


	function change() {

		$user	= $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
		
			foreach ($user as $a) {
				$data = [
					'image' 		=> $user['image'],
					'nama' 			=> $user['nama'],
					'email' 		=> $user['email'],
					'alamat'		=> $user['alamat'],
					'tanggal_input' => $user['tanggal_input'],
					];
			}

			$this->form_validation->set_rules('nama', 'Nama Lengkap',
				'required|trim', [
					'required' => 'Nama ngga boleh kosong.'
			]);

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Profil Saya';
			$this->load->view('inc/header', $data);
			$this->load->view('inc/navbar', $data);
			$this->load->view('_member/change', $data);
			$this->load->view('inc/footer', $data);
		} else {

			$data = [
					'nama' 		=> $this->input->post('nama',true),
					'alamat' 	=> htmlspecialchars($this->input->post('alamat',true)),
					// 'email' 	=> $this->input->post('email',true),
				];
				
			$upload_image = $_FILES['image']['name'];

				if ($upload_image) {

					$config['upload_path'] 		= './assets/img/profile/';
					$config['allowed_types'] 	= 'gif|jpg|png';
					$config['file_name'] 		= '_' . date('His');
					$this->load->library('upload', $config);
			
				if ($this->upload->do_upload('image')) {
					
					$gambar_lama = $data['nama']['image'];

						if ($gambar_lama != 'default.png') {
							unlink(FCPATH . 'assets/img/profile/' . $gambar_lama);
						}

						$gambar_baru = $this->upload->data('file_name');
							$this->db->set('image', $gambar_baru);
						} else { }
					
				}

			$this->db->set($data);
			$this->db->where('email', $this->input->post('email', true));
			$this->db->update('user');

			$this->session->set_flashdata('message', 
				'<div class="alert alert-info alert-dismissible fade show" role="alert">
			<i class="fas fa-user-circle"></i> Profil berhasil diperbarui..
		  	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>');

			redirect('member/profile');
		}

	}
}
