<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		
		parent::__construct();

		cek_login();
	}

	function index() {

		$data['title'] = 'My Profile';
		$data['user'] = $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('_admin/inc/header', $data);
		$this->load->view('_admin/inc/sidebar', $data);
		$this->load->view('_admin/inc/topbar', $data);
		$this->load->view('_admin/user/index', $data);
		$this->load->view('_admin/inc/footer');
	}

	function anggota() {

		$this->form_validation->set_rules('nama', 'Nama Lengkap',
			'required', [
				'required' 	=> 'Nama belum di isi.'
			]);

			$this->form_validation->set_rules('email', 'Alamat Email',
			'required|trim|valid_email|is_unique[user.email]', [
				'valid_email' 	=> 'Email nya ngga bener nih.',
				'required' 		=> 'Email belum di isi.',
				'is_unique' 	=> 'Email sudah terdaftar, gunakan email lain.'
			]);

			$this->form_validation->set_rules('password', 'Password',
			'required|trim|min_length[3]', [
				'required' 		=> 'Password belum di isi.',
				'min_length' 	=> 'Password terlalu pendek'
			]);

			$this->form_validation->set_rules('role_id', 'Role Model',
			'required|trim', [
				'required' 		=> 'Pilih role model.'
			]);

		if ($this->form_validation->run() == false) {

			$data['title'] = 'Data Anggota';
			$data['user'] = $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
			// $this->db->where('role_id', 1);

			$data['anggota'] = $this->db->get('user')->result_array();

				$this->load->view('_admin/inc/header', $data);
				$this->load->view('_admin/inc/sidebar');
				$this->load->view('_admin/inc/topbar');
				$this->load->view('_admin/user/anggota', $data);
				$this->load->view('_admin/inc/footer');

		} else {

			$email = $this->input->post('email', true);

				$config['upload_path'] = './assets/img/profile/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name'] = '_' . date('His');
				$this->load->library('upload', $config);
				$this->upload->do_upload('image');
			
				$data = [
					'nama' 			=> htmlspecialchars($this->input->post('nama',true)),
					'email' 		=> htmlspecialchars($email),
					'image' 		=> 'default.png',
					'password' 		=> password_hash($this->input->post('password'), PASSWORD_DEFAULT),
					'role_id' 		=> $this->input->post('role_id',true),
					'is_active' 	=> 1,
					'alamat'		=> htmlspecialchars($this->input->post('alamat',true)),
					'image'			=> $this->upload->data('file_name'),
					'tanggal_input' => time()
				];
				
			$this->ModelUser->simpanData($data);
			$this->session->set_flashdata('message', '<div class="alert alert-info alert-message" role="alert">Anggota baru berhasil ditambahkan.</div>');
		redirect('user/anggota');
		}

	}


	function ubahProfil() {

		$data['title'] 	= 'Ubah Profil';
		$data['user'] 	= $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
		
		$this->form_validation->set_rules('nama', 'Nama Lengkap',
			'required|trim', [
				'required' => 'Nama tidak boleh kosong'
		]);

		if ($this->form_validation->run() == false) {
			$this->load->view('_admin/inc/header', $data);
			$this->load->view('_admin/inc/sidebar', $data);
			$this->load->view('_admin/inc/topbar', $data);
			$this->load->view('_admin/user/change', $data);
			$this->load->view('_admin/inc/footer');
		} else {

			$nama 	= $this->input->post('nama', true);
			$email 	= $this->input->post('email', true);
			$upload_image = $_FILES['image']['name'];

				if ($upload_image) {

					$config['upload_path'] = './assets/img/profile/';
					$config['allowed_types'] = 'gif|jpg|png';
					// $config['max_size'] = '3000';
					// $config['max_width'] = '1024';
					// $config['max_height'] = '1000';
					$config['file_name'] = '_' . date('His');

					$this->load->library('upload', $config);
			
				if ($this->upload->do_upload('image')) {
					
					$gambar_lama = $data['user']['image'];

						if ($gambar_lama != 'default.png') {
							unlink(FCPATH . 'assets/img/profile/' . $gambar_lama);
						}

						$gambar_baru = $this->upload->data('file_name');
							$this->db->set('image', $gambar_baru);
						} else { }
					
				}

			$this->db->set('nama', $nama);
			$this->db->where('email', $email);
			$this->db->update('user');

			$this->session->set_flashdata('message', 
				'<div class="alert alert-info alert-message" role="alert"><i class="fas fa-check-circle"></i>
				Profil has been updated..
				</div>');

			redirect('user');
		}
	}



	function change() {

		$data['title'] 	= 'Ubah Profil';
		$data['user'] 	= $this->ModelUser->getUserWhere(['id' => $this->uri->segment(3)])->row_array();
		
		$this->form_validation->set_rules('nama', 'Nama Lengkap',
			'required|trim', [
				'required' => 'Nama tidak boleh kosong'
		]);

		if ($this->form_validation->run() == false) {
			$this->load->view('_admin/inc/header', $data);
			$this->load->view('_admin/inc/sidebar', $data);
			$this->load->view('_admin/inc/topbar', $data);
			$this->load->view('_admin/user/change', $data);
			$this->load->view('_admin/inc/footer');
		} else {

			

			$data = array(
			        'nama' 		=> $this->input->post('nama', true),
			        // 'email'  => $this->input->post('email', true),
			        'role_id'  	=> $this->input->post('role_id', true),
			        'is_active' => $this->input->post('is_active', true),
			        'alamat'  	=> $this->input->post('alamat', true)
				);

			$upload_image = $_FILES['image']['name'];

				if ($upload_image) {

					$config['upload_path'] = './assets/img/profile/';
					$config['allowed_types'] = 'gif|jpg|png';
					// $config['max_size'] = '3000';
					// $config['max_width'] = '1024';
					// $config['max_height'] = '1000';
					$config['file_name'] = '_' . date('His');
					$this->load->library('upload', $config);
			
				if ($this->upload->do_upload('image')) {
					
					$gambar_lama = $data['user']['image'];

						if ($gambar_lama != 'default.png') {
							unlink(FCPATH . 'assets/img/profile/' . $gambar_lama);
						}

						$gambar_baru = $this->upload->data('file_name');
							$this->db->set('image', $gambar_baru);
						} else { }
					
				}

			$this->db->set($data);
			$this->db->where('id', $this->input->post('user_id'));
			$this->db->update('user');

			$this->session->set_flashdata('message', 
				'<div class="alert alert-info alert-message" role="alert"><i class="fas fa-check-circle"></i>
				Data user berhasil di update..
				</div>');

			redirect('user/anggota');
		}
	}


	function hapusUser() {
		$where = ['id' => $this->uri->segment(3)];
		$gambar_lama = $data['user']['image'];

		$this->ModelUser->hapusUser($where);

		if ($gambar_lama != 'default.png') {
				unlink(FCPATH . 'assets/img/profile/' . $gambar_lama);
			}

		$this->session->set_flashdata('message', '<div class="alert alert-info alert-message" role="alert"><i class="fas fa-check-circle"></i> User berhasil di hapus.</div>');
		redirect('user/anggota');
	}


	
}
