<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends CI_Controller {

	function __construct() {
		parent::__construct();
		cek_login();
		cek_user();
	}

	function index() {

		$data['title'] 		= 'Dashboard';
		$data['user'] 		= $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
		$data['anggota'] 	= $this->ModelUser->getUserLimit()->result_array();
		$data['buku'] 		= $this->ModelBuku->getLimitBuku()->result_array();
	}

	function buku() {

		$data['title'] 		= 'Laporan Data Buku';
		$data['user'] 		= $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
		$data['buku'] 		= $this->ModelBuku->getBuku()->result_array();
		$data['kategori'] 	= $this->ModelBuku->getKategori()->result_array();

		$this->load->view('_admin/inc/header', $data);
		$this->load->view('_admin/inc/sidebar', $data);
		$this->load->view('_admin/inc/topbar', $data);
		$this->load->view('_admin/buku/laporanBuku', $data);
		$this->load->view('_admin/inc/footer');
	}

	function bukuCetak() {

		$data['buku'] 		= $this->ModelBuku->getBuku()->result_array();
		$data['kategori'] 	= $this->ModelBuku->getKategori()->result_array();
		$this->load->view('_admin/buku/laporanBukuPrint', $data);
	}

	function bukuPDF() {

		$data['buku'] 	= $this->ModelBuku->getBuku()->result_array();
		$sroot 			= $_SERVER['DOCUMENT_ROOT'];
		include $sroot . "/pustaka/application/third_party/dompdf/autoload.inc.php";
		$dompdf 		= new Dompdf\Dompdf();

		$this->load->view('_admin/buku/laporanBukuPDF', $data);
		
		$paper_size 	= 'A4';
		$orientation 	= 'landscape';
		$html 			= $this->output->get_output();
		$dompdf->set_paper($paper_size, $orientation);
		
		$dompdf->load_html($html);
		$dompdf->render();
		$dompdf->stream("laporan_data_buku.pdf", array('Attachment' => 0)); // nama file pdf yang di hasilkan
	}

	function bukuExcel() {
		$data = array(
			'title' => 'Laporan Buku',
			'buku'	=> $this->ModelBuku->getBuku()->result_array());
		$this->load->view('_admin/buku/laporanBukuExcel', $data);
	}

// ********************************************************************************************


	function pinjam() {

		$data['title'] 		= 'Laporan Data Peminjaman';
		$data['user'] 		= $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
		$data['laporan'] 	= $this->db->query("select * from pinjam p,detail_pinjam d, buku b,user u where d.id_buku=b.id and p.id_user=u.id and p.no_pinjam=d.no_pinjam")->result_array();

		$this->load->view('_admin/inc/header', $data);
		$this->load->view('_admin/inc/sidebar', $data);
		$this->load->view('_admin/inc/topbar', $data);
		$this->load->view('_admin/booking/laporanPinjam', $data);
		$this->load->view('_admin/inc/footer');

	}

	function pinjamCetak() {
		$data['title'] 		= 'Laporan Data Peminjaman';
		$data['laporan'] = $this->db->query("select * from pinjam p,detail_pinjam d, buku b,user u where d.id_buku=b.id and p.id_user=u.id and p.no_pinjam=d.no_pinjam")->result_array();
		$this->load->view('_admin/booking/laporanPinjamPrint', $data);
	}


	function pinjamPDF() {

		$data['laporan'] = $this->db->query("select * from pinjam p,detail_pinjam d, buku b,user u where d.id_buku=b.id and p.id_user=u.id and p.no_pinjam=d.no_pinjam")->result_array();

		$sroot 		= $_SERVER['DOCUMENT_ROOT'];
		include $sroot . "/pustaka/application/third_party/dompdf/autoload.inc.php";
		$dompdf 	= new Dompdf\Dompdf();

		$this->load->view('_admin/booking/laporanPinjamPDF', $data);

		$paper_size = 'A4';
		$orientation = 'landscape';
		$html = $this->output->get_output();
		$dompdf->set_paper($paper_size, $orientation);

		$dompdf->load_html($html);
		$dompdf->render();
		$dompdf->stream("laporan data peminjaman.pdf", array('Attachment' => 0));
	}

	function pinjamExcel() {

		$data = array( 'title' => 'Laporan Data Peminjaman Buku', 'laporan' => $this->db->query("select * from pinjam p,detail_pinjam d, buku b,user u where d.id_buku=b.id and p.id_user=u.id and p.no_pinjam=d.no_pinjam")->result_array());
		$this->load->view('_admin/booking/laporanPinjamExcel', $data);
	}

// ********************************************************************************************


	function anggota() {

		$data['title'] 		= 'Laporan Data Anggota';
		$data['user'] 		= $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
		$data['anggota'] 	= $this->db->get('user')->result_array();
		// $data['laporan'] 	= $this->db->query("select * from pinjam p,detail_pinjam d, buku b,user u where d.id_buku=b.id and p.id_user=u.id and p.no_pinjam=d.no_pinjam")->result_array();

		$this->load->view('_admin/inc/header', $data);
		$this->load->view('_admin/inc/sidebar', $data);
		$this->load->view('_admin/inc/topbar', $data);
		$this->load->view('_admin/user/laporanAnggota', $data);
		$this->load->view('_admin/inc/footer');
	}

	function anggotaCetak() {
		$data['title'] 		= 'Laporan Data Anggota';
		$data['anggota'] 	= $this->db->get('user')->result_array();
		$this->load->view('_admin/user/laporanAnggotaPrint', $data);
	}

	function anggotaPDF() {

		$data['anggota'] = $this->db->get('user')->result_array();
		$sroot 		= $_SERVER['DOCUMENT_ROOT'];
		include $sroot . "/pustaka/application/third_party/dompdf/autoload.inc.php";
		$dompdf 	= new Dompdf\Dompdf();

		$this->load->view('_admin/user/laporanAnggotaPDF', $data);

		$paper_size = 'A4';
		$orientation = 'landscape';
		$html = $this->output->get_output();
		$dompdf->set_paper($paper_size, $orientation);

		$dompdf->load_html($html);
		$dompdf->render();
		$dompdf->stream("laporan anggota.pdf", array('Attachment' => 0));
	}

	function anggotaExcel() {

		$data = array(
			'title' 	=> 'Laporan Data Anggota',
			'anggota'	=> $this->db->get('user')->result_array());
		$this->load->view('_admin/user/laporanAnggotaExcel', $data);
	}


}