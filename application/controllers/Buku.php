<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Buku extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		cek_login();
		cek_user();
	}

function index() {

	$data['title'] 		= 'Data Buku';
	$data['user'] 		= $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
	$data['buku'] 		= $this->ModelBuku->tampil()->result_array();
	$data['kategori'] 	= $this->ModelBuku->getKategori()->result_array();

		$this->form_validation->set_rules('judul_buku', 'Judul Buku',
		'required|min_length[3]', [
			'required' 		=> 'Judul Buku harus diisi',
			'min_length' 	=> 'Judul buku terlalu pendek'
		]);

		$this->form_validation->set_rules('id_kategori', 'Kategori',
		'required', [
			'required' 		=> 'Nama pengarang harus diisi',
		]);

		$this->form_validation->set_rules('pengarang', 'Nama Pengarang', 'required|min_length[3]', [
			'required' 		=> 'Nama pengarang harus diisi',
			'min_length' 	=> 'Nama pengarang terlalu pendek'
		]);


		$this->form_validation->set_rules('penerbit', 'Nama Penerbit', 'required|min_length[3]', [
			'required' 		=> 'Nama penerbit harus diisi',
			'min_length' 	=> 'Nama penerbit terlalu pendek'
		]);

		$this->form_validation->set_rules('tahun_terbit', 'Tahun Terbit',
			'required|max_length[4]|numeric', [
				'required' 		=> 'Tahun terbit harus diisi',
				'max_length' 	=> 'Tahun terbit terlalu panjang',
				'numeric' 		=> 'Hanya boleh diisi angka'
		]);

		$this->form_validation->set_rules('isbn', 'Nomor ISBN',
			'required|min_length[3]|numeric', [
				'required' 		=> 'Nama ISBN harus diisi',
				'min_length' 	=> 'Nama ISBN terlalu pendek',
				'numeric' 		=> 'Yang anda masukan bukan angka'
		]);

		$this->form_validation->set_rules('stok', 'Stok',
			'required|numeric', [
				'required' 	=> 'Stok harus diisi',
				'numeric' 	=> 'Yang anda masukan bukan angka'
		]);

	
	$config['upload_path'] = './assets/img/upload/';
	$config['allowed_types'] = 'jpg|png|jpeg';
	// $config['max_size'] = '3000';
	// $config['max_width'] = '1024';
	// $config['max_height'] = '1000';
	$config['file_name'] = 'book_' . time();

	$this->load->library('upload', $config);

	if ($this->form_validation->run() == false) {
		$this->load->view('_admin/inc/header', $data);
		$this->load->view('_admin/inc/sidebar', $data);
		$this->load->view('_admin/inc/topbar', $data);
		$this->load->view('_admin/buku/index', $data);
		$this->load->view('_admin/inc/footer');
	
	} else {

		if ($this->upload->do_upload('image')) {
			$image = $this->upload->data();
			$gambar = $image['file_name'];
		} else {
			$gambar = '';
		}
		$data = [

				'judul_buku' 	=> $this->input->post('judul_buku', true),
				'id_kategori' 	=> $this->input->post('id_kategori', true),
				'pengarang' 	=> $this->input->post('pengarang', true),
				'penerbit' 		=> $this->input->post('penerbit', true),
				'tahun_terbit' 	=> $this->input->post('tahun_terbit', true),
				'isbn' 			=> $this->input->post('isbn', true),
				'stok' 			=> $this->input->post('stok', true),
				'dipinjam' 		=> 0,
				'dibooking' 	=> 0,
				'image' 		=> $gambar
			];
		$this->ModelBuku->simpanBuku($data);
		$this->session->set_flashdata('message', '<div class="alert alert-info alert-message" role="alert"><i class="fas fa-check-circle"></i> Buku baru berhasil ditambahkan.</div>');

		redirect('buku');
		}
	}



	function kategori() {

		$data['title'] = 'Kategori Buku';
		$data['user'] = $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
		$data['kategori'] = $this->ModelBuku->getKategori()->result_array();

		$this->form_validation->set_rules('kategori', 'Kategori', 'required', [
			'required' => 'Kategori Buku harus diisi.'
		]);


		if ($this->form_validation->run() == false) {
			$this->load->view('_admin/inc/header', $data);
			$this->load->view('_admin/inc/sidebar', $data);
			$this->load->view('_admin/inc/topbar', $data);
			$this->load->view('_admin/buku/category', $data);
			$this->load->view('_admin/inc/footer');
		} else {
			$data = ['kategori' => $this->input->post('kategori') ];
			$this->ModelBuku->simpanKategori($data);
			redirect('buku/kategori');
		}
	}


	function ubahKategori() {

		$data['title'] = 'Ubah Kategori Buku';
		$data['user'] = $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
		$data['kategori'] 	= $this->ModelBuku->kategoriWhere(['id' => $this->uri->segment(3)])->row_array();

		$this->form_validation->set_rules('kategori', 'Kategori', 'required', [
			'required' => 'Kategori Buku harus diisi.'
		]);


		if ($this->form_validation->run() == false) {
			$this->load->view('_admin/inc/header', $data);
			$this->load->view('_admin/inc/sidebar', $data);
			$this->load->view('_admin/inc/topbar', $data);
			$this->load->view('_admin/buku/changeCategory', $data);
			$this->load->view('_admin/inc/footer');
		} else {
			$data = ['kategori' => $this->input->post('kategori', TRUE) ];
			$this->ModelBuku->updateKategori($data, ['id' => $this->input->post('id_kategori')]);
			$this->session->set_flashdata('message', '<div class="alert alert-info alert-message" role="alert"><i class="fas fa-check-circle"></i> Kategori berhasil diubah.</div>');
			redirect('buku/kategori');
		}
	}


	function hapusKategori() {
		$where = ['id' => $this->uri->segment(3)];
		$this->ModelBuku->hapusKategori($where);
		$this->session->set_flashdata('message', '<div class="alert alert-info alert-message" role="alert"><i class="fas fa-check-circle"></i> Kategori berhasil dihapus.</div>');
		redirect('buku/kategori');
	}



	function ubahBuku() {

			$data['title'] 	= 'Ubah Data Buku';
			$data['user'] 	= $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
			$data['buku'] 	= $this->ModelBuku->bukuWhere(['id' => $this->uri->segment(3)])->row_array();
			$kategori 		= $this->ModelBuku->joinKategoriBuku(['buku.id' => $this->uri->segment(3)])->result_array();
		
		foreach ($kategori as $k) {
			$data['id'] 	= $k['id_kategori'];
			$data['k'] 		= $k['kategori'];
		}

		$data['kategori'] = $this->ModelBuku->getKategori()->result_array();

		$this->form_validation->set_rules('judul_buku', 'Judul Buku',
			'required|min_length[3]', [
				'required'	 => 'Judul Buku harus diisi',
				'min_length' => 'Judul buku terlalu pendek'
		]);
		$this->form_validation->set_rules('id_kategori', 'Kategori',
		'required', [
			'required' 		=> 'Nama pengarang harus diisi',
		]);
		$this->form_validation->set_rules('pengarang', 'Nama
		Pengarang', 'required|min_length[3]', [
			'required' 		=> 'Nama pengarang harus diisi',
			'min_length' 	=> 'Nama pengarang terlalu pendek'
		]);
		$this->form_validation->set_rules('penerbit', 'Nama Penerbit',
		'required|min_length[3]', [
			'required' 		=> 'Nama penerbit harus diisi',
			'min_length' 	=> 'Nama penerbit terlalu pendek'
		]);
		$this->form_validation->set_rules('tahun_terbit', 'Tahun Terbit',
		'required|max_length[4]|numeric', [
			'required' 		=> 'Tahun terbit harus diisi',
			'max_length' 	=> 'Tahun terbit terlalu panjang',
			'numeric' 		=> 'Hanya boleh diisi angka'
		]);
		$this->form_validation->set_rules('isbn', 'Nomor ISBN',
		'required|min_length[3]|numeric', [
			'required' 		=> 'Nama ISBN harus diisi',
			'min_length' 	=> 'Nama ISBN terlalu pendek',
			'numeric' 		=> 'Yang anda masukan bukan angka'
		]);
		$this->form_validation->set_rules('stok', 'Stok',
		'required|numeric', [
			'required' 		=> 'Stok harus diisi',
			'numeric' 		=> 'Yang anda masukan bukan angka'
		]);

		$config['upload_path'] 		= './assets/img/upload/';
		$config['allowed_types'] 	= 'jpg|png|jpeg';
		// $config['max_size'] 		= '3000';
		// $config['max_width'] 	= '1024';
		// $config['max_height'] 	= '1000';
		$config['file_name'] 		= 'book_' . time();

		$this->load->library('upload', $config);

		if ($this->form_validation->run() == false) {
			$this->load->view('_admin/inc/header', $data);
			$this->load->view('_admin/inc/sidebar', $data);
			$this->load->view('_admin/inc/topbar', $data);
			$this->load->view('_admin/buku/change', $data);
			$this->load->view('_admin/inc/footer');
		} else {

			if ($this->upload->do_upload('image')) {
				$image = $this->upload->data();
				unlink('assets/img/upload/' . $this->input->post('old_pict', TRUE));
				$gambar = $image['file_name'];
			} else {
				$gambar = $this->input->post('old_pict', TRUE);
			}

		$data = [
			'judul_buku' 	=> $this->input->post('judul_buku', true),
			'id_kategori' 	=> $this->input->post('id_kategori', true),
			'pengarang' 	=> $this->input->post('pengarang', true),
			'penerbit' 		=> $this->input->post('penerbit', true),
			'tahun_terbit' 	=> $this->input->post('tahun_terbit', true),
			'isbn' 			=> $this->input->post('isbn', true),
			'stok' 			=> $this->input->post('stok', true),
			'image' 		=> $gambar
		];

		$this->ModelBuku->updateBuku($data, ['id' => $this->input->post('id_buku')]);
		$this->session->set_flashdata('message', '<div class="alert alert-info alert-message" role="alert"><i class="fas fa-check-circle"></i> Data buku berhasil diubah.</div>');
		redirect('buku');
		}
		
	}

	function hapusBuku() {

		$where = ['id' => $this->uri->segment(3)];

		if(!empty($where)) {
			$this->db->select('image');
			$this->db->from('buku');
			$this->db->where('id', $this->uri->segment(3));

			$query = $this->db->get();
			$data = $query->row_object();

			$file = FCPATH.'/assets/img/upload/'.$data->image;

			if (file_exists($file)) {

                if (unlink($file)) {
					$this->session->set_flashdata('message', '<div class="alert alert-info alert-message" role="alert"><i class="fas fa-check-circle"></i> Gambar berhasil dihapus.</div>');
                } else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger alert-message" role="alert"><i class="fas fa-exclamation-triangle"></i> Gambar tidak berhasil dihapus.</div>');
                }
            } else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-message" role="alert"><i class="fas fa-exclamation-triangle"></i> Gambar tidak tersedia.</div>');
            }

		}
		
		$this->ModelBuku->hapusBuku($where);
		
		$this->session->set_flashdata('message', '<div class="alert alert-info alert-message" role="alert"><i class="fas fa-check-circle"></i> Data buku berhasil dihapus.</div>');
		redirect('buku');
	}

}

